import React, { Fragment, useEffect, useState } from 'react';
import { Button, Card, Divider, message, Modal, Popconfirm, Table, Tag } from 'antd';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import SearchForm from '@/components/SearchForm';
import styles from '@/utils/table.less';
import { TestItem } from './data.d';
import { Dispatch, ConnectState } from '@/models/connect';
import { ColumnProps, PaginationConfig, SorterResult } from 'antd/es/table';


import { connect } from 'dva';
import encodeQueryParam from '@/utils/encodeParam';

//interface用于限定props属性
interface Props {
    test: any;  //字段
    dispatch: Dispatch;
    location: Location;
    loading: boolean;
}

interface State {
    data: any;
    searchParam: any;
    saveVisible: boolean;
    currentItem: Partial<TestItem>;
    saveLoading: boolean;
    autzVisible: boolean;
}

//props.test导入数据.怎么实现?
//大括号被解释伪代码块，所以如果箭头函数直接返回一个对象，必须在对象外边加上括号,类似一个转义，箭头函数本身默认return
//组件定义
const testList: React.FC<Props> = props => {
//const test = (props)=>{
    console.log(props);
    const { dispatch } = props;
    //const { result } = props.test;
    const { result } ={result:{}};

    const initState: State = {
        data: result,
        searchParam: { pageSize: 10 },
        saveVisible: false,
        saveLoading: false,
        currentItem: {},
        autzVisible: false,
    };

    const [searchParam, setSearchParam] = useState(initState.searchParam);

    const columns: ColumnProps<TestItem>[] = [  //表列
        {
            title: '姓名',
            dataIndex: 'name',
        },
        {
            title: '用户名',
            dataIndex: 'username',
        },
        {
            title: '状态',
            dataIndex: 'status',
            render: text => (
                <Tag color={text === 1 ? '#108ee9' : '#f50'}>{text === 1 ? '正常' : '已禁用'}</Tag>
            ),
        },
        {
            title: '操作',
            render: (text, record) => (
                <Fragment>
                    <a onClick={() => edit(record)}>编辑</a>
                    <Divider type="vertical" />

                    <a onClick={() => setting(record)}>赋权</a>
                    <Divider type="vertical" />
                    {record.status !== 1 ? (
                    <span>
                        <Popconfirm
                            title="确认启用此用户？"
                            onConfirm={() => {
                  enableOrDisable(record);
                }}
                        >
                            <a>启用</a>
                        </Popconfirm>
                        <Divider type="vertical" />
                        <a onClick={() => handleDelete(record)}>删除</a>
                    </span>
                        ) : (
                    <Popconfirm
                        title="确认禁用此用户？"
                        onConfirm={() => {
                enableOrDisable(record);
              }}
                    >
                        <a>禁用</a>
                    </Popconfirm>
                        )}
                </Fragment>
            ),
        },
    ];


    const handleSearch = (params?: any) => {
        setSearchParam(params);
        dispatch({
            type: 'users/query',
            payload: encodeQueryParam(params),
        });
    };


    const onTableChange = (
        pagination: PaginationConfig,
        filters: any,
        sorter: SorterResult<any>,
        extra: any,
    ) => {
        handleSearch({
            pageIndex: Number(pagination.current) - 1 || 0,
            pageSize: pagination.pageSize,
            terms: searchParam.terms,
            sorts: sorter,
        });
    };

    return (

        <PageHeaderWrapper title="测试菜单(PageHeaderWrapper为菜单第一个标签,包括面包屑和标题)">
            <Card bordered={false}>
                <div>card 表示内容页</div>
                <div className={styles.tableList}>
                    <div>
                        <div>
                            <SearchForm
                                search={(params: any) => {
                setSearchParam(params);
                handleSearch({ terms: params, pageSize: 10 });
              }}
                                formItems={[
                {
                  label: '姓名',
                  key: 'name$LIKE',
                  type: 'string',
                },
                {
                  label: '用户名',
                  key: 'username$LIKE',
                  type: 'string',
                },
              ]}
                            />
                        </div>
                        <div className={styles.StandardTable}>
                            <Table
                                loading={props.loading}
                                dataSource={result?.data}
                                columns={columns}
                                rowKey="id"
                                onChange={onTableChange}
                                pagination={{
                                  current: result.pageIndex + 1,
                                  total: result.total,
                                  pageSize: result.pageSize,
                                  showQuickJumper: true,
                                  showSizeChanger: true,
                                  pageSizeOptions: ['10', '20', '50', '100'],
                                  showTotal: (total: number) =>
                                    `共 ${total} 条记录 第  ${result.pageIndex + 1}/${Math.ceil(
                                      result.total / result.pageSize,
                                    )}页`,
                                }}
                            />
                        </div>

                    </div>
                </div>
            </Card>
        </PageHeaderWrapper>
    );
}
export default connect(({ test, loading }: ConnectState) => ({
    test,
    loading: loading.models.users,
}))(testList);